# Trusted Repository for Ultimate tool family 
This repository contains binaries of the [Ultimate](https://github.com/ultimate-pa/ultimate) tools that can be used together with various actors of [CoVeriTeam](https://gitlab.com/sosy-lab/software/coveriteam).
